﻿using UnityEngine;
using System.Collections;

public class Gaffer : MonoBehaviour {

	public Camera Cam ;
	public Transform target;



	public float smooth;
	public float posYcam;


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void FixedUpdate()
	{
		transform.position=Vector3.Lerp(transform.position,target.position,smooth);
		Cam.transform.position=Vector3.Lerp(Cam.transform.position,new Vector3(Cam.transform.position.x,posYcam,Cam.transform.position.z),smooth);

	}
}
