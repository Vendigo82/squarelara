﻿using UnityEngine;
using System.Collections;

public class Platforms : MonoBehaviour {

	public Direction direction = Direction.HORIZONTAL;

	public Transform _pointA;
	public Transform _pointB;

	public enum Direction { NONE, HORIZONTAL, VERTICAL};
	public enum TypePlatform { NONE, MANUAL, FROM_STEP};

	void Start () {
	
	}

	public void MovesPlatform() 
	{
		if (transform.position == _pointA.transform.position) {
			transform.position = new Vector3 (_pointB.transform.position.x, _pointB.transform.position.y, _pointB.transform.position.z);
		} else {

			if (transform.position == _pointB.transform.position) {
				transform.position = new Vector3 (_pointA.transform.position.x, _pointA.transform.position.y, _pointA.transform.position.z);
			}
		}
	}

	void Update () {
	}
}
