﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class AnimatorHelper {

    public static IEnumerator waitState(Animator animator, int layer, string state) {
        while ( animator.IsInTransition(layer) || 
            (! animator.GetCurrentAnimatorStateInfo(layer).IsName(state)) ) {
            yield return null;
        }
    }

    public static IEnumerator waitStates(Animator animator, int layer, string[] states) {
        while ( animator.IsInTransition(layer) || (! checkStates(animator.GetCurrentAnimatorStateInfo(layer), states) ) ) {
            yield return null;
        }
    }

    private static bool checkStates(AnimatorStateInfo stateInfo, string[] states) {
        foreach (string s in states) {
            if (stateInfo.IsName(s))
                return true;
        }
        return false;
    }
}
