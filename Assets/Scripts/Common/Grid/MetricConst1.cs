﻿using System.Collections;
using System.Collections.Generic;

namespace VenSoft.Grid {

    /**
     * Metric, always return 1
     */
    public class MetricConst1<TNode> : MetricI<TNode> where TNode : NodeI<TNode> {
        public float distance(TNode n1, TNode n2) {
            return 1;
        }
    }

}