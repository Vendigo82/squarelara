﻿using System.Collections;
using System.Collections.Generic;

namespace VenSoft.Grid {

    /**
     * Functor, calculate distance beetwen two nodes
     */
    public interface MetricI<TNode> where TNode : NodeI<TNode> {

        /**
         * return distance beetwen n1 and n2
         */
        float distance(TNode n1, TNode n2);
    }

}