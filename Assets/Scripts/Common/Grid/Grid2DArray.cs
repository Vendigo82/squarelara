﻿namespace VenSoft.Grid {

    /// <summary>
    /// implementation of GridI interface, based on 2D array
    /// </summary>
    /// <typeparam name="TCell"> type of cell</typeparam>
    public class Grid2DArray<TNode> : GridI<TNode> 
        where TNode : NodeI<TNode> {

        //two-dimensional array of cells, first index is x-dimension, second index is y-dimension
        private TNode[,] cells;
        //offset for x indexes
        private readonly int xIndexOffset = 0;
        //offset for y indexes
        private readonly int yIndexOffset = 0;
        //
        private readonly int zIndex;

        public Grid2DArray(int xMin, int yMin, int xMax, int yMax, int zIndex) {
            xIndexOffset = xMin;
            yIndexOffset = yMin;
            this.zIndex = zIndex;

            int xSize = xMax - xMin + 1;
            int ySize = yMax - yMin + 1;

            cells = new TNode[xSize, ySize];
        }

        public Grid2DArray(int xMin, int yMin, int xMax, int yMax, int zIndex, NodeFactory<TNode> factory) {
            xIndexOffset = xMin;
            yIndexOffset = yMin;
            this.zIndex = zIndex;

            int xSize = xMax - xMin + 1;
            int ySize = yMax - yMin + 1;

            cells = new TNode[xSize, ySize];
            createNodes(factory);
        }

        public void createNodes(NodeFactory<TNode> factory) {            
            for (int x = 0; x < cells.GetLength(0); ++x)
                for (int y = 0; y < cells.GetLength(1); ++y)
                    cells[x, y] = factory.create(x, y, this.zIndex);            
        }

        public TNode getNode(GridPosition pos) {
            return getNode(pos.x, pos.y, pos.z);
        }

        public TNode getNode(int x, int y, int z) {
            if (z != zIndex)
                return default(TNode);
            if (x < xIndexOffset)
                return default(TNode);
            if (y < yIndexOffset)
                return default(TNode);

            if (x + xIndexOffset >= cells.GetLength(0))
                return default(TNode);
            if (y + yIndexOffset >= cells.GetLength(1))
                return default(TNode);

            return cells[x + xIndexOffset, y + yIndexOffset];
        }
    }

}