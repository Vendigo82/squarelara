﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace VenSoft.Grid {

    public class PathFinderAStartTester {

        private bool[,] cells;
        private Grid grid;

        private static float MAX_WEIGHT = 1000;

        public void init() {
            cells = new bool[5,5];
            for (int i = 0; i < cells.GetLength(0); ++i)
                for (int j = 0; j < cells.GetLength(1); ++j)
                    cells[i, j] = true;
            cells[0, 2] = false;
            cells[1, 2] = false;
            cells[2, 2] = false;
            cells[2, 1] = false;

            grid = new Grid(cells);
        }

        public void testCells() {            
            Node n = grid.getNode(0,0,0);
            IEnumerator<Node> item = n.GetEnumerator();
            item.MoveNext();
            assertNode(item.Current, 0, 1);
            item.MoveNext();
            assertNode(item.Current, 1, 0);
            if (item.MoveNext())
                throw new Exception();
        }

        public void testWay() {            
            PathFinderI<Node> pf = new PathFinderAStar<Node>(new NeighbourMetric(), new Metric(), MAX_WEIGHT);
            calcPath(pf, 0, 0, 4, 4);
        }

        public void assertNode(Node n, int row, int col) {
            if (n.indexX != col || n.indexY != row)
                throw new Exception(String.Format("Node[{0},{1}] have not indexes {2}, {3}", n.indexX, n.indexY, col, row));
        }

        public void calcPath(PathFinderI<Node> pf, int r1, int c1, int r2, int c2) {
            Debug.Log(string.Format("From {0},{1} to {2},{3}", r1, c1, r2, c2));
            ICollection<Node> path = pf.findPath(grid.getNode(r1,c1,0), grid.getNode(r2,c2,0));
            if (path != null)
                LogPath(path);
            else
                Debug.Log("path not found");
        }

        public void LogPath(ICollection<Node> path) {            
            Debug.Log("Path:");
            foreach(Node n in path) {
                Node node = (Node)n;
                Debug.Log(string.Format("{0},{1}", node.indexX, node.indexY));
            }
        }

        private class Metric : MetricI<Node> {
            public float distance(Node n1, Node n2) {
                float x = n1.indexX - n2.indexX;
                float y = n1.indexY - n2.indexY;
                return Mathf.Sqrt(x*x + y*y);
            }
        }

        private class NeighbourMetric : MetricI<Node> {
            public float distance(Node n1, Node n2) {
                if (n2.isOpen())
                    return 1;
                else
                    return MAX_WEIGHT;
            }
        }

        public class MyNodeFactory : NodeFactory<Node> {
            private readonly Grid grid;

            public MyNodeFactory(Grid grid) {
                this.grid = grid;
            }

            public Node create(int x, int y, int z) {
                return new Node(grid, x, y, z);
            }
        }

        public class Grid : Grid2DArray<Node> {
            private bool[,] cells;

            public Grid(bool[,] cells)
                : base(0, 0, cells.GetLength(0)-1, cells.GetLength(1)-1, 0) {
                this.cells = cells;
                createNodes(new MyNodeFactory(this));
            }

            public bool isOpen(int row, int col) {
                return cells[row, col];
            }
        }

        public class Node : Node4<Node> {
            private Grid grid;
            public Node(Grid grid, int x, int y, int z)
                : base(grid, x, y, z) {
                this.grid = grid;
            }

            public bool isOpen() {
                return grid.isOpen(indexX, indexY);
            }
        }

    }
   
}