﻿using System.Collections.Generic;

namespace VenSoft.Grid {
    
    /**
     * Graph node with neighbours nodes with weights   
     */ 
    public interface NodeI<TNode> : IEnumerable<TNode> where TNode : NodeI<TNode> {               
    }

}