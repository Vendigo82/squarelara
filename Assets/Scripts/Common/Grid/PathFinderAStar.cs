﻿using System.Collections.Generic;

namespace VenSoft.Grid {

    /**
     * PathFinder, make algorith A*
     */
    public class PathFinderAStar<TNode> : PathFinderI<TNode> where TNode : NodeI<TNode>  {

        private MetricI<TNode> weightFunc;  //hypotetical distance 
        private MetricI<TNode> hypMetric;  //hypotetical distance         
        private float maxWeight;

        //nodes to review and hipotetical way cost = way cost + mextrix; Ordered by hyp waycost
        private HashSet<TNode> openNodes = null;
        //review complete nodes
        private HashSet<TNode> closeNodes = null;
        //current waycost from started node to key-node
        private Dictionary<TNode, float> wayCost = null;
        //hypotetical waycost from key-node to finish
        private Dictionary<TNode, float> hypCost = null;
        //parents
        private Dictionary<TNode, TNode> parents = null;

        /**
         * Constructor
         * @param weightFunc - weight between beighbours nodes
         * @param hypMetric - hypotetical weight from one node to finish node
         * @param maxWeight - maximum path length or 0 for unlimited
         */
        public PathFinderAStar(MetricI<TNode> weightFunc, MetricI<TNode> hypMetric, float maxWeight)  {
            this.weightFunc = weightFunc;
            this.hypMetric = hypMetric;            
            this.maxWeight = maxWeight;
        }

        /**
         * PathFinderI interface
         * search path from start to end and return nodes in path, or return null, if path not found
         */
        public ICollection<TNode> findPath(TNode start, TNode finish) {
            prepare(start, finish);
            while(openNodes.Count > 0) {
                TNode current = extractNodeMin();
                //reached finish, make path and return
                if (current.Equals(finish))
                    return createPath(current);
                else
                    makeStep(current, finish);
            }
            //not found
            return null;
        }

        /**
         * first step of algorithm
         */
        public void prepare(TNode start, TNode finish) {
            initCollections();
            openNodes.Add(start);
            wayCost[start] = 0;
            hypCost[start] = hypMetric.distance(start, finish);
        }

        /**
         * extract node from OpenNodes with minimal hypotetical distance
         */
        public TNode extractNodeMin() {
            TNode nodeMin = default(TNode);
            float minWeight = 0;
            bool first = true;
            foreach (TNode n in openNodes) {
                float w = hypCost[n];
                if (first || w < minWeight) {
                    nodeMin = n;
                    minWeight = w;
                    first = false;
                } 
            }
                
            openNodes.Remove(nodeMin);
            closeNodes.Add(nodeMin);
            return nodeMin;
        }

        /**
         * perform step
         */
        public void makeStep(TNode current, TNode finish) {
            foreach(TNode n in current) {
                float newWayCost = wayCost[current] + weightFunc.distance(current, n);// current.getWeight(n);
                bool inCloses = closeNodes.Contains(n);
                if (inCloses && newWayCost >= wayCost[n])
                    continue;
                if (!inCloses || newWayCost < wayCost[n]) {
                    parents[n] = current;
                    wayCost[n] = newWayCost;
                    hypCost[n] = newWayCost + hypMetric.distance(n, finish);
                    if (maxWeight == 0 || newWayCost <= maxWeight)
                        openNodes.Add(n);
                }
            }
        }

        /**
         * create path use parents bookmarks
         */
        public ICollection<TNode> createPath(TNode current) {
            LinkedList<TNode> result = new LinkedList<TNode>();
            while (true) {
                result.AddFirst(current);
                if (parents.ContainsKey(current))
                    current = parents[current];
                else
                    return result;
            }
        }

        /**
         * Create new or clear existing collections
         */ 
        private void initCollections() {
            if (openNodes == null)
                openNodes = new HashSet<TNode>();
            else
                openNodes.Clear();

            if (closeNodes == null)
                closeNodes = new HashSet<TNode>();
            else
                closeNodes.Clear();

            if (wayCost == null)
                wayCost = new Dictionary<TNode, float>();
            else
                wayCost.Clear();     

            if (parents == null)
                parents = new Dictionary<TNode, TNode>();
            else
                parents.Clear();

            if (hypCost == null)
                hypCost = new Dictionary<TNode, float>();
            else
                hypCost.Clear();
        }            

    }        

}