﻿using System.Collections;
using System.Collections.Generic;

namespace VenSoft.Grid {

    /**
     * Find path on graph from one node to another
     */
    public interface PathFinderI<TNode> where TNode : NodeI<TNode> {
        ICollection<TNode> findPath(TNode start, TNode finish);
    }

}