﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace VenSoft.Grid {
    /// <summary>
    /// Grid node with four directions: up, down, left, right
    /// </summary>
    /// <typeparam name="TNode">real type</typeparam>
    public class Node4<TNode> : NodeI<TNode> where TNode : NodeI<TNode> {

        //reference to grid
        private readonly GridI<TNode> m_grid;
        //x index in grid
        private readonly int m_x;
        //y index in grid
        private readonly int m_y;
        //z index
        private readonly int m_z;

        public int indexX { get { return m_x; } }
        public int indexY { get { return m_y; } }
        public int indexZ { get { return m_z; } }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="grid">parent grid</param>
        /// <param name="x">x index in grid</param>
        /// <param name="y">y index in grid</param>
        public Node4(GridI<TNode> grid, int x, int y, int z) {
            m_grid = grid;
            m_x = x;
            m_y = y;
            m_z = z;
        }

        public IEnumerator<TNode> GetEnumerator() {
            return new Node4Enumerator(this);
        }

        IEnumerator IEnumerable.GetEnumerator() {
            return new Node4Enumerator(this);
        }

        /// <summary>
        /// Enumerator for neighbour nodes
        /// </summary>
        private class Node4Enumerator : IEnumerator<TNode> {
            //index offsets for neighbour nodes: right, up, left, down
            private static readonly int[,] offset = new int[,] { {1, 0}, {0, 1}, {-1, 0}, {0, -1} };
            //parent node
            private readonly Node4<TNode> m_node;   
            //current index in offset arrays
            private int index = -1;
            private TNode current = default(TNode);

            public Node4Enumerator(Node4<TNode> node) {
                m_node = node;
            }

            public TNode Current {
                get {
                    return current;
                }
            }

            object IEnumerator.Current {
                get {
                    return current;
                }
            }

            public void Dispose() {                
            }

            public bool MoveNext() {
                do {
                    index += 1;
                    if (index >= offset.GetLength(0))
                        return false;

                    current = m_node.m_grid.getNode(m_node.m_x + offset[index, 0], m_node.m_y + offset[index, 1], m_node.m_z);
                } while (current == null);
                return true;
            }

            public void Reset() {
                index = -1;
            }
        }
    }
}
