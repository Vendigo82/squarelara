﻿namespace VenSoft.Grid {

    /// <summary>
    /// Interface for gaming grid
    /// </summary>
    /// <typeparam name="TNode">type of cell</typeparam>
    public interface GridI<TNode> where TNode : NodeI<TNode> {

        /// <summary>
        /// return WayPoint for specified position or null
        /// </summary>
        /// <param name="pos">grid position</param>
        /// <returns>WayPoint or null</returns>
        TNode getNode(GridPosition pos);

        /// <summary>
        /// return WayPoint for specified position or null
        /// </summary>
        /// <param name="x">x index</param>
        /// <param name="y">y index</param>
        /// <param name="z">z index</param>
        /// <returns>WayPoint or null</returns>
        TNode getNode(int x, int y, int z);
    }

}