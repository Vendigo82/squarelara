﻿namespace VenSoft.Grid {

    /// <summary>
    /// struct for three indexes pack
    /// </summary>
    public struct GridPosition {

        public GridPosition(int x, int y, int z) {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        public int x;
        public int y;
        public int z;

        public static bool operator ==(GridPosition p1, GridPosition p2) {
            return (p1.x == p2.x) && (p1.y == p2.y) && (p1.z == p2.z);
        }

        public static bool operator !=(GridPosition p1, GridPosition p2) {
            return (p1.x != p2.x) || (p1.y != p2.y) || (p1.z != p2.z);
        }

        public override bool Equals(object o) {
            if (o is GridPosition) {
                GridPosition p = (GridPosition)o;
                if (p == this)
                    return true;

                return (x == p.x) && (y == p.y) && (z == p.z);
            }
            return false;
        }

        public override int GetHashCode() {
            return x + y + z;
        }
    }

}