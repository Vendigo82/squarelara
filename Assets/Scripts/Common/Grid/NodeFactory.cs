﻿namespace VenSoft.Grid {
    public interface NodeFactory<TNode> 
        where TNode : NodeI<TNode> {
            TNode create(int x, int y, int z);
        }   
}
