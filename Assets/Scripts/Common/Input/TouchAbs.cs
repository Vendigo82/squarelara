﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class TouchAbs : MonoBehaviour {
    public abstract void onTouchDown(Ray ray, RaycastHit hit);
    public abstract void onTouch(Ray rayDown, Ray moveRay);
    public abstract void onTouchUp(Ray rayDown, Ray rayUp);
}
