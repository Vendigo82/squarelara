﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchClickable : TouchAbs {

    public delegate void OnClickHandler(TouchClickable sender);
    public event OnClickHandler OnClick;
       
    public override void onTouchDown(Ray ray, RaycastHit hit) {
    }

    public override void onTouch(Ray rayDown, Ray moveRay) {
    }

    public override void onTouchUp(Ray rayDown, Ray rayUp) {
        RaycastHit hit;
        if (Physics.Raycast(rayUp, out hit)) {
            if (hit.transform == transform)
                OnClickEvent();
        }
    }

    protected void OnClickEvent() {        
        if (OnClick != null)
            OnClick(this);
    }
}
