﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Touch manager
 */
public class TouchManager : MonoBehaviour {

    //current touched item
    private TouchAbs m_currentItem = null;
    private Ray rayDown;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetMouseButtonDown(0))
            onDown();
        if (Input.GetMouseButtonUp(0))
            onUp();
        if (Input.GetMouseButton(0))
            onMove();
	}

    private void onDown() {
        m_currentItem = null;

        rayDown = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(rayDown, out hit)) {
            TouchAbs item = hit.transform.GetComponent<TouchAbs>();
            if (item != null && item.enabled) {
                m_currentItem = item;
                m_currentItem.onTouchDown(rayDown, hit);
            }
        }
    }

    private void onUp() {
        if (m_currentItem != null) {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            m_currentItem.onTouchUp(rayDown, ray);
            m_currentItem = null;
        }
    }

    private void onMove() {
        if (m_currentItem != null) {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            m_currentItem.onTouch(rayDown, ray);
        }
    }
}
