﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Any trigger, have list of action and can perform thoose actions
 */
public class Trigger : GameBaseBehaviour {

    //list of actions
    public Action [] actions;

    //perform actions
    public void performActions() {
        foreach(Action a in actions) {
            if (a.canPerformAction())
                a.performAction();
        }
    }
}
