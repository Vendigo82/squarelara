﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Button, use with TouchClickable
 * when button touched, then perform all Actions components
 */
public class ClickableButton : Trigger {

    void OnEnable() {
        TouchClickable touch = GetComponent<TouchClickable>();
        touch.OnClick += onClick;
    }

    void OnDisable() {
        TouchClickable touch = GetComponent<TouchClickable>();
        touch.OnClick -= onClick;
    }

    private void onClick(TouchClickable sender) {
        if (! isPlayerInAccesibleRange() )
            return;
        
        performActions();
    }

    private bool isPlayerInAccesibleRange() {
        return Level.inst.player.gridItem.getCurrentWayPoint() == gridItem.getCurrentWayPoint();
    }
}
