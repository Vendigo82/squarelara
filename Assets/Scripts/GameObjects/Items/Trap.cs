﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trap : Trigger, GameItem {

    public Renderer []rendersForDisable;
    private bool trapIsActive = false;

    /**
     * Called when any GridItem entered in current WayPoint
     */
    /*protected override void onEnterItem(GridItem item) {
        if (trapIsActive)
            performActions();
    }

    protected override void onExitItem(GridItem item) {
       if (item == Level.inst.player) {
            //WayPoint wp = getCurrentWayPoint();
            //if (wp != null) {
              //  wp.enabled = false;
                foreach (Renderer r in rendersForDisable)
                    r.enabled = false;
            trapIsActive = true;
            //} else
              //  Debug.LogError(name + " not found current way point");
        }
    }*/

    /// <summary>
    /// return true, if GameItem can enter in WayPoint owned by this GameItem
    /// </summary>
    /// <param name="item"></param>
    public bool requestEnterItem(GameItem item) {
        return true;
    }

    /// <summary>
    /// Called when any GameItem entered in WayPoint owned by this GameItem
    /// </summary>
    /// <param name="item">entered GameItem</param>
    public void onEnterItem(GameItem item) {
        if (trapIsActive)
            performActions();
    }

    /// <summary>
    /// Called when any GridItem exit from WayPoint owned by this GameItem
    /// </summary>
    /// <param name="item">exit GameItem</param>
    public void onExitItem(GameItem item) {
        if (item == Level.inst.player.gameItem) {
            foreach (Renderer r in rendersForDisable)
                r.enabled = false;
            trapIsActive = true;
        }
    }

    /// <summary>
    /// permanent death
    /// </summary>
    public void kill() {

    }

    /// <summary>
    /// return true if item can be killed from WayPoint wp
    /// </summary>
    /// <param name="wp">from this WayPoint item to be attack</param>
    /// <returns></returns>
    public bool canBeKilled(WayPoint wp) {
        return false;
    }
}
