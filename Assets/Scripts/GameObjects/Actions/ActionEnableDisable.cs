﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Action switch enable/disable any MonoBehavior
 */
public class ActionEnableDisable : Action {

    public MonoBehaviour [] subjects;

    /// <summary>
    /// calling when action started
    /// </summary>
    /// <returns>true if need set actionInProgress flag to true </returns>
    protected override void onStartAction() {
        foreach (MonoBehaviour mb in subjects) {
            mb.enabled = ! mb.enabled;
        }
        completeAction();
    }
}
