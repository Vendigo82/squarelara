﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionPlatform : Action {

    //if true, then positionA = transform.position
    public bool useStartingPositionAsA = true;
    //postion A
    public Vector3 positionA;
    //position B
    public Vector3 positionB;

    //active waypoints in positionA
    public WayPoint []wayPointsA;
    //active waypoints in  positionB
    public WayPoint []wayPointsB;

    //moving speed
    public float speed;

    //true, if current position is A
    private bool currentPosA = true;

	// Use this for initialization
	void Start () {
        if (useStartingPositionAsA)
            positionA = transform.localPosition;
        setWayPointsEnable(wayPointsA, true);
        setWayPointsEnable(wayPointsB, false);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    /// <summary>
    /// calling when action started
    /// </summary>
    /// <returns>true if need set actionInProgress flag to true </returns>
    protected override void onStartAction() {
        StartCoroutine(performMove());        
    }

    /**
     * Perform moving
     */
    private IEnumerator performMove() {
        if (currentPosA) {
            setWayPointsEnable(wayPointsA, false);
            yield return StartCoroutine(performMoveTo(positionB));
            setWayPointsEnable(wayPointsB, true);
            currentPosA = false;
        } else {
            setWayPointsEnable(wayPointsB, false);
            yield return StartCoroutine(performMoveTo(positionA));
            setWayPointsEnable(wayPointsA, true);
            currentPosA = true;
        }        
        completeAction();
    }

    /**
     * Perform moving to position
     */ 
    private IEnumerator performMoveTo(Vector3 pos) {        
        while (transform.position != pos) {
            transform.localPosition = Vector3.MoveTowards(transform.localPosition, pos, Time.deltaTime * speed);
            yield return null;
        }
    }

    /**
     * Enable or disable array of WayPoints
     */
    private void setWayPointsEnable(WayPoint []wayPoints, bool enable) {
        foreach(WayPoint wp in wayPoints)
            wp.enabled = enable;
    }

}
