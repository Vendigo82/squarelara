﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Call kill for any objects on WayPoint
 */ 
public class ActionKillOnWayPoint : Action {
    public WayPoint wpKill;

    /// <summary>
    /// calling when action started
    /// </summary>
    /// <returns>true if need set actionInProgress flag to true </returns>
    protected override void onStartAction() {
        LinkedList<GameItem> items = wpKill.getItems();
        foreach (GameItem item in items) {
            item.kill();
        }
        completeAction();
    }
}
