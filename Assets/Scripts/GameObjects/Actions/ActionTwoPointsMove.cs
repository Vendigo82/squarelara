﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 1. Moving between two points
/// 2. Update his childs WayPoints
/// </summary>
public class ActionTwoPointsMove : Action {

    public Transform pointA;
    public Transform pointB;

    public float Speed = 10;

    //true if object at pointA position
    private bool m_atPostionA;    

    private const float DISTANCE_EPS = 0.01f;

    void Awake() {
        float distToA = Vector3.Distance(transform.position, pointA.position);
        m_atPostionA =  distToA < DISTANCE_EPS;
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (isActionInProgress()) {
            Vector3 to;
            if (m_atPostionA)
                to = pointB.position;
            else
                to = pointA.position;
            transform.position = Vector3.MoveTowards(transform.position, to, Speed * Time.deltaTime);
            if (transform.position == to) {
                m_atPostionA = !m_atPostionA;                
                Level.inst.grid.updateChildWayPointsForObject(gameObject);
                completeAction();
            }
        }
	}

}
