﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Game action
/// </summary>
public abstract class Action : MonoBehaviour {

    //true - if action in progress
    private bool m_actionInProgess = false;

    /// <summary>
    /// perform action
    /// </summary>
    public void performAction() {
        m_actionInProgess = true;
        Level.inst.blockGame();
        onStartAction();            
    }

    /// <summary>
    /// action is perfoming or idle flag
    /// </summary>
    /// <returns>true if action is perfoming, or false</returns>
    public bool isActionInProgress() {
        return m_actionInProgess;
    }

    /// <summary>
    /// can perform action or not
    /// </summary>
    /// <returns>true if can perform action</returns>
    public bool canPerformAction() {
        return ! isActionInProgress();
    }

    /// <summary>
    /// calling when action started
    /// </summary>
    /// <returns>true if need set actionInProgress flag to true </returns>
    protected virtual void onStartAction() {
    }

    /// <summary>
    /// need to call, when action is complete performing
    /// </summary>
    protected void completeAction() {
        if (m_actionInProgess)
            Level.inst.unblockGame();
        m_actionInProgess = false;
    }
}
