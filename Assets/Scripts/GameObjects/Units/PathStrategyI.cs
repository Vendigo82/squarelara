﻿
/// <summary>
/// interface for path selecting strategy
/// </summary>
public interface PathStrategyI {

    /// <summary>
    /// get next way point in path
    /// </summary>
    /// <returns>WayPoint or null</returns>
    WayPoint getNextWayPoint();

}
