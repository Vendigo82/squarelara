﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Enemy : TurnBased, GameItem {
    /// <summary>
    /// movement speed
    /// </summary>
    public float movingSpeed = 10;

    /// <summary>
    /// rotating speed
    /// </summary>
    public float rotateSpeed = 360;

    private bool m_alive = true;

    void OnEnable() {
        gridItem.onStopMovingEvent += onStopMovingEvent;
    }

    void OnDisable() {
        gridItem.onStopMovingEvent -= onStopMovingEvent;
    }

    /// <summary>
    /// GameItem interface
    /// return true, if GameItem can enter in WayPoint owned by this GameItem
    /// </summary>
    /// <param name="item"></param>
    public bool requestEnterItem(GameItem item) {
        return ! m_alive;
    }

    /// <summary>
    /// GameItem interface
    /// Called when any GameItem entered in WayPoint owned by this GameItem
    /// </summary>
    /// <param name="item">entered GameItem</param>
    public void onEnterItem(GameItem item) {

    }

    /// <summary>
    /// GameItem interface
    /// Called when any GridItem exit from WayPoint owned by this GameItem
    /// </summary>
    /// <param name="item">exit GameItem</param>
    public void onExitItem(GameItem item) {

    }

    /// <summary>
    /// GameItem interface
    /// permanent death
    /// </summary>
    public void kill() {
        if (m_alive) {
            m_alive = false;
            Debug.Log(name + " was killed");
            StartCoroutine(death());
        }
    }

    private IEnumerator death() {
        HumanAnimation anim = GetComponent<HumanAnimation>();
        yield return StartCoroutine(anim.death());
        Debug.Log(name + " need destroy");
        DestroyGameItem();
    }

    /// <summary>
    /// GameItem interface
    /// return true if item can be killed from WayPoint wp
    /// </summary>
    /// <param name="wp">from this WayPoint item to be attack</param>
    /// <returns></returns>
    public bool canBeKilled(WayPoint wp) {
        return m_alive;
    }

    /// <summary>
    /// TurnBased interface
    /// called when Item's turn was started
    /// </summary>
    protected override void onTurnStarted() {
        base.onTurnStarted();

        WayPoint playerWP = Level.inst.player.gridItem.getCurrentWayPoint();
        if (gridItem.getLookingDirection() == gridItem.getCurrentWayPoint().getDirectionForPoint(playerWP)) {
            StartCoroutine(killPlayer(playerWP));
        }
        else
            turnOver();
    }

    private IEnumerator killPlayer(WayPoint playerWP) {
        yield return StartCoroutine(GetComponent<HumanAnimation>().shoot(1));
        Level.inst.player.gameItem.kill();
        gridItem.rotateAndMoveToImmediatly(playerWP, movingSpeed, rotateSpeed);
    }

    /// <summary>
    /// Called when movement was finished
    /// </summary>
    /// <param name="sender"></param>
    private void onStopMovingEvent(GridItem sender) {
        Level.inst.turnOver(this);
    }
}
