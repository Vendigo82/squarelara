﻿using UnityEngine;

/// <summary>
/// Grid orientation strategy for Saw
/// </summary>
public class SawGridOrientation : MonoBehaviour, GridOrientationStrategy {
    private GridOrientationStrategy strategy = new GridOrientationStrategyNorth();

    public float getAngle(Vector3 cur, Vector3 target) {
        float angle = strategy.getAngle(cur, target);
        float curAngle = transform.rotation.eulerAngles.y;

        while (angle - curAngle >= 180)
            angle -= 180;
        while (angle - curAngle <= -180)
            angle += 180;

        return angle;
    }

    public WayPoint.DIRECTION getLookingDirection(Vector3 eulerAngler) {
        return strategy.getLookingDirection(eulerAngler);
    }
}
