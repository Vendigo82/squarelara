﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class for Unit controlled by Player
/// </summary>
public class Player : GameBaseBehaviour, TurnBasedI, GameItem {
    /// <summary>
    /// movement speed
    /// </summary>
    public float movingSpeed = 10;

    /// <summary>
    /// rotating speed
    /// </summary>
    public float rotateSpeed = 360;

    private bool m_listenInput = false;

    void OnEnable() {
        gridItem.onStopMovingEvent += onStopMovingEvent;
    }

    void OnDisable() {
        gridItem.onStopMovingEvent -= onStopMovingEvent;
    }

    // Update is called once per frame
    protected void Update () {
        if (! gridItem.idle)
            return;

        if (Level.inst.isCanMovePlayer() && m_listenInput) {
            WayPoint next = getTargetWayPoint();

            if (next != null) {
                m_listenInput = false;
                moveToWayPoint(next);
            }
        }                            
	}

    /// <summary>
    /// TurnBased interface implementation
    /// </summary>
    public void startTurn() {
        m_listenInput = true;
        Debug.Log("player's turn");
    }

    /// <summary>
    /// GameItem interface implementation
    /// return true, if GameItem can enter in WayPoint owned by this GameItem
    /// </summary>
    /// <param name="item"></param>
    public bool requestEnterItem(GameItem item) {
        return true;
    }

    /// <summary>
    /// /// GameItem interface implementation
    /// Called when any GameItem entered in WayPoint owned by this GameItem
    /// </summary>
    /// <param name="item">entered GameItem</param>
    public void onEnterItem(GameItem item) {

    }

    /// <summary>
    /// GameItem interface implementation
    /// Called when any GridItem exit from WayPoint owned by this GameItem
    /// </summary>
    /// <param name="item">exit GameItem</param>
    public void onExitItem(GameItem item) {

    }

    /// <summary>
    /// GameItem interface implementation
    /// permanent death
    /// </summary>
    public void kill() {
        Debug.Log("Player was killed, lol");
        //gridItem.replaceImmeadiatly(Level.inst.lastRespawn.getCurrentWayPoint());
        StartCoroutine(death());
    }

    private IEnumerator death() {
        HumanAnimation anim = GetComponent<HumanAnimation>();
        yield return StartCoroutine(anim.death());
        yield return StartCoroutine(anim.ressurect());
        gridItem.replaceImmeadiatly(Level.inst.lastRespawn.getCurrentWayPoint());
        //anim.animator.
        //DestroyGameItem();
    }

    /// <summary>
    /// GameItem interface implementation
    /// return true if item can be killed from WayPoint wp
    /// </summary>
    /// <param name="wp">from this WayPoint item to be attack</param>
    /// <returns></returns>
    public bool canBeKilled(WayPoint wp) {
        return true;
    }

    /// <summary>
    /// Called when movement was finished
    /// </summary>
    /// <param name="sender"></param>
    private void onStopMovingEvent(GridItem sender) {
        Level.inst.turnOver(this);
    }

    /// <summary>
    /// listen user input and return WayPoint for move to
    /// </summary>
    /// <returns>WayPoint or null</returns>
    private WayPoint getTargetWayPoint () {
        float x = Input.GetAxis("Horizontal");
        float y = Input.GetAxis("Vertical");

        if (x != 0 && y != 0)
            //same time pressed couple directions, ignoring
            return null;

        //WayPoint cur = Level.inst.grid.getWayPoint(currentPosition);
        WayPoint next = null;
        WayPoint cur = gridItem.getCurrentWayPoint();
        if (x != 0 && y == 0) {
            if (x < 0)
                next = Level.inst.grid.getWestWP(cur);
            else
                next = Level.inst.grid.getEastWP(cur);
        }
        else if (x == 0 && y != 0) {
            if (y < 0)
                next = Level.inst.grid.getSouthWP(cur);
            else
                next = Level.inst.grid.getNorthWP(cur);
        }
        return next;
    }

    /// <summary>
    /// perform move to another waypoint
    /// </summary>
    /// <param name="next">WayPoint to move</param>
    /// <returns>true if started action, can not accept other commands</returns>
    private bool moveToWayPoint(WayPoint next) {
        LinkedList<GameItem> items = next.getItems();

        bool canPerformMove = true;
        bool needShoot = false;

        //kill items
        foreach (GameItem item in items) {
            if (item.canBeKilled(gridItem.getCurrentWayPoint()))
                needShoot = true;
            else if (!item.requestEnterItem(this))
                canPerformMove = false;

            //if (!(item.canBeKilled(gridItem.getCurrentWayPoint()) || item.requestEnterItem(this) )) {
              //  canPerformMove = false;
            //}
        }
        if (!canPerformMove)
            return false;

        if (needShoot)
            StartCoroutine(killEnemies(next, items));
        else
            gridItem.rotateAndMoveToImmediatly(next, movingSpeed, rotateSpeed);
        return true;
    }

    private IEnumerator killEnemies(WayPoint next, LinkedList<GameItem> items) {
        gridItem.rotateToImmeadiatly(next, rotateSpeed);
        yield return StartCoroutine(gridItem.waitIdle());

        yield return StartCoroutine(GetComponent<HumanAnimation>().shoot(1));

        //kill items
        foreach (GameItem item in items) {
            if (item.canBeKilled(gridItem.getCurrentWayPoint())) {
                item.kill();
            }
        }

        gridItem.moveToImmediatly(next, movingSpeed);
    }

}
