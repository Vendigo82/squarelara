﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HumanAnimation : MonoBehaviour {

    public Animator animator;

    private const int DEATH_LEVEL = 4;

    private const string DEATH_LEVEL_DEAD_01_STATE = "Dead_01";
    private const string DEATH_LEVEL_DEAD_02_STATE = "Dead_02";
    private const string DEATH_LEVEL_ALIVE_STATE = "Alive";

    private const int WEAPONS_LEVEL = 6;

    private const string WEAPONS_LEVEL_HANDGUN_IDLE = "Character_Handgun_Idle";

    private const string P_SPEED_F = "Speed_f";
    private const string P_DEATH_B = "Death_b";
    private const string P_SHOOT_B = "Shoot_b";

    // Use this for initialization
    void Start () {
        animator.SetFloat(P_SPEED_F, 0);
    }

    void OnEnable() {
        GridItem gridItem = GetComponent<GridItem>();
        gridItem.onStartMovingEvent += onStartWalking;
        gridItem.onStopMovingEvent += onStopWalking;
    }

    void OnDisable() {
        GridItem gridItem = GetComponent<GridItem>();
        gridItem.onStartMovingEvent -= onStartWalking;
        gridItem.onStopMovingEvent -= onStopWalking;
    }

    private void onStartWalking(GridItem sender) {        
        animator.SetFloat(P_SPEED_F, 1);
    }

    private void onStopWalking(GridItem sender) {        
        animator.SetFloat(P_SPEED_F, 0);
    }

    public IEnumerator death() {
        animator.SetBool(P_DEATH_B, true);
        yield return StartCoroutine(AnimatorHelper.waitStates(animator, DEATH_LEVEL, 
            new string[] { DEATH_LEVEL_DEAD_01_STATE, DEATH_LEVEL_DEAD_02_STATE }));
    }

    public IEnumerator ressurect() {
        animator.SetBool(P_DEATH_B, false);
        yield return StartCoroutine(AnimatorHelper.waitState(animator, DEATH_LEVEL, DEATH_LEVEL_ALIVE_STATE));
        animator.transform.localPosition = Vector3.zero;
    }

    public IEnumerator shoot(float tm) {
        animator.SetBool(P_SHOOT_B, true);
        if (tm != 0)
        yield return new WaitForSeconds(tm);
        animator.SetBool(P_SHOOT_B, false);
        yield return StartCoroutine(AnimatorHelper.waitState(animator, WEAPONS_LEVEL, WEAPONS_LEVEL_HANDGUN_IDLE));
    }

    void Update() {
    }
}
