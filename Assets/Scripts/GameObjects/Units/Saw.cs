﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Saw : TurnBased, GameItem {
    public float movingSpeed = 10;
    public float rotateSpeed = 360;

    private PathStrategyI m_pathStrategy = null;

    void Awake() {
        m_pathStrategy = GetComponent<PathStrategyI>();
        if (m_pathStrategy == null)
            throw new System.Exception(name + " not found PathStrategy");
    }

    void OnEnable() {
        gridItem.onStopMovingEvent += onStopMovingEvent;
    }

    void OnDisable() {
        gridItem.onStopMovingEvent -= onStopMovingEvent;
    }

    protected override void onTurnStarted() {            
        WayPoint wp = m_pathStrategy.getNextWayPoint();
        if (wp != null) {
            WayPoint.DIRECTION dir = gridItem.getCurrentWayPoint().getDirectionForPoint(wp);
            if (dir != WayPoint.DIRECTION.NULL)
                gridItem.rotateAndMoveToImmediatly(wp, movingSpeed, rotateSpeed);
            else
                turnOver();
        }
        else
            turnOver();
    }

    /// <summary>
    /// gridItem.onStopMovingEvent
    /// called when Saw complete movement
    /// </summary>
    /// <param name="sender"></param>
    private void onStopMovingEvent(GridItem sender) {
        //first kill all items in Saw position
        LinkedList<GameItem> items = gridItem.getCurrentWayPoint().getItems();
        foreach (GameItem item in items)
            item.kill();

        //then turn over
        turnOver();
    }

    /// <summary>
    /// GameItem interface
    /// return true, if GameItem can enter in WayPoint owned by this GameItem
    /// </summary>
    /// <param name="item"></param>
    public bool requestEnterItem(GameItem item) {
        return true;
    }

    /// <summary>
    /// GameItem interface
    /// Called when any GameItem entered in WayPoint owned by this GameItem
    /// </summary>
    /// <param name="item">entered GameItem</param>
    public void onEnterItem(GameItem item) {
        //any item enter in Saw must be killed
        item.kill();
    }

    /// <summary>
    /// GameItem interface
    /// Called when any GridItem exit from WayPoint owned by this GameItem
    /// </summary>
    /// <param name="item">exit GameItem</param>
    public void onExitItem(GameItem item) {

    }

    /// <summary>
    /// GameItem interface
    /// permanent death
    /// </summary>
    public void kill() {
        //saw can not be killed
    }

    /// <summary>
    /// return true if item can be killed from WayPoint wp
    /// </summary>
    /// <param name="wp">from this WayPoint item to be attack</param>
    /// <returns></returns>
    public bool canBeKilled(WayPoint wp) {
        return false;
    }
}
