﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// path strategy with fixed WayPoint's
/// </summary>
public class PathStrategyFixed : GameBaseBehaviour, PathStrategyI {

    /// <summary>
    /// array of WayPoints
    /// </summary>
    public List<WayPoint> path;

    //current WayPoint index in path
    private int m_currentPathIndex = -1;
    //moving direction: 1 or -1
    private int m_direction = 1;

    // Use this for initialization
    void Start () {
        WayPoint itemWP = gridItem.getCurrentWayPoint();
        for (int i = 0; i < path.Count; ++i) {
            if (path[i] == itemWP) {
                m_currentPathIndex = i;
                //break;
            }
        }
	}

    public WayPoint getNextWayPoint() {
        if (path.Count <= 1)
            return null;

        //if next point for current direction is accesible
        if (isNextPointAccesible(m_direction)) {
            m_currentPathIndex = m_currentPathIndex + m_direction;
            return path[m_currentPathIndex];
        } else if (isNextPointAccesible(-m_direction)) { 
            m_direction = -m_direction;
            m_currentPathIndex = m_currentPathIndex + m_direction;
            return path[m_currentPathIndex];
        }

        return null;
    }

    private bool isNextPointAccesible(int dir) {
        if (m_currentPathIndex + dir >= 0 && m_currentPathIndex + dir < path.Count)
            if (gridItem.getCurrentWayPoint().getDirectionForPoint(path[m_currentPathIndex + dir]) != WayPoint.DIRECTION.NULL)
                return true;
        return false;
    }
}
