﻿using UnityEngine;

public interface GridOrientationStrategy {
    float getAngle(Vector3 cur, Vector3 target);

    WayPoint.DIRECTION getLookingDirection(Vector3 eulerAngler);
}
