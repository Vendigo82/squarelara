﻿using UnityEngine;

/// <summary>
/// interface for communication with game  world objects
/// </summary>
public interface GameItem {

    /// <summary>
    /// return true, if GameItem can enter in WayPoint owned by this GameItem
    /// </summary>
    /// <param name="item"></param>
    bool requestEnterItem(GameItem item);

    /// <summary>
    /// Called when any GameItem entered in WayPoint owned by this GameItem
    /// </summary>
    /// <param name="item">entered GameItem</param>
    void onEnterItem(GameItem item);

    /// <summary>
    /// Called when any GridItem exit from WayPoint owned by this GameItem
    /// </summary>
    /// <param name="item">exit GameItem</param>
    void onExitItem(GameItem item);

    /// <summary>
    /// permanent death
    /// </summary>
    void kill();

    /// <summary>
    /// return true if item can be killed from WayPoint wp
    /// </summary>
    /// <param name="wp">from this WayPoint item to be attack</param>
    /// <returns></returns>
    bool canBeKilled(WayPoint wp);
}
