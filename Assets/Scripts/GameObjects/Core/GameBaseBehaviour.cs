﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameBaseBehaviour : MonoBehaviour {

    public GridItem gridItem {
        get {
            return GetComponent<GridItem>();
        }
    }

    public TurnBasedI turnBased {
        get {
            return GetComponent<TurnBasedI>();
        }
    }

    public GameItem gameItem {
        get {
            return GetComponent<GameItem>();
        }
    }

    protected void DestroyGameItem() {
        if (gameItem != null && gridItem != null)
            gridItem.getCurrentWayPoint().onExit(gameItem);
        if (turnBased != null)
            Level.inst.unregisterTurnBasedItem(turnBased);
        GameObject.DestroyObject(gameObject);
    }
}
