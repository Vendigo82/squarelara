﻿using UnityEngine;

public class GridOrientationStrategyNorth : GridOrientationStrategy {
    
    //item looking to north angle
    private const float ANGLE_TO_NORTH = 0;

    public float getAngle(Vector3 cur, Vector3 target) {        
        Vector3 d = target- cur;
        if (d.magnitude == 0)
            return 0;
        d.Normalize();
        float angle;
        if (d.x >= 0)
            angle = Mathf.Acos(d.z);
        else
            angle = -Mathf.Acos(d.z);
        angle = angle * Mathf.Rad2Deg;
        while (angle < 0)
            angle += 360;
        while (angle >= 360)
            angle -= 360;
        return angle;
    }

    public WayPoint.DIRECTION getLookingDirection(Vector3 eulerAngler) {
        float angle = eulerAngler.y - ANGLE_TO_NORTH;
        while (angle < 0)
            angle = 360 + angle;
        int d = Mathf.RoundToInt(angle / 90);

        switch (d) {
            case 0:
                return WayPoint.DIRECTION.NORTH;
            case 1:
                return WayPoint.DIRECTION.EAST;
            case 2:
                return WayPoint.DIRECTION.SOUTH;
            default:
                return WayPoint.DIRECTION.WEST;
        }
    }
}
