﻿using UnityEngine;

/// <summary>
/// Game object behavior
/// implements turn-based logic: start turn, end turn, etc
/// </summary>
public class TurnBased : GameBaseBehaviour, TurnBasedI {

    public delegate void OnTurnStartedHandler();
    public event OnTurnStartedHandler onTurnStartedEvent;

    protected void Start() {
        Level.inst.registerTurnBasedItem(this);
    }

    /// <summary>
    /// start item's turn
    /// </summary>
    public void startTurn() {
        onTurnStarted();
        if (onTurnStartedEvent != null)
            onTurnStartedEvent();
    }

    /// <summary>
    /// need call, when turn is over
    /// </summary>
    protected void turnOver() {
        Level.inst.turnOver(this);
    }

    /// <summary>
    /// verify now is items turn or not
    /// </summary>
    /// <returns>true - if now is item's turn; false - otherwise</returns>
    protected bool isTurnStarted() {
        return Level.inst.isItemTurn(this);
    }

    /// <summary>
    /// calls when item's turn is started
    /// </summary>
    protected virtual void onTurnStarted() {

    }

}
