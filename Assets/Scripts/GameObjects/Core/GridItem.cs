﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GridPosition = VenSoft.Grid.GridPosition;

/**
 * Some game item, placed on gird position
 */
public class GridItem : GameBaseBehaviour {

    //current waypoint
    private WayPoint m_currentWP = null;
    //current moving to position, null if not moving.
    private WayPoint m_targetWP = null;
    
    //moving speed
    private float m_moveSpeed;
    //before moving rotation complete or not
    private bool m_completeRotation = true;
    //rotate target
    private Quaternion rotateTo;
    //rotate speed in degrees
    private float m_rotateSpeed;

    private GridOrientationStrategy m_gridOrientation;

    public delegate void OnMotionHandler(GridItem sender);
    public event OnMotionHandler onStartMovingEvent;
    public event OnMotionHandler onStopMovingEvent;
    public event OnMotionHandler onStartRotationEvent;
    public event OnMotionHandler onStopRotationEvent;

    /**
     * true - if item is moving now
     */
    //public bool isMovingInProgress {
      //  get {return m_targetWP != null; }
    //}

    public bool idle {
        get {
            return m_targetWP == null || m_completeRotation == false;
        }
    }

    public IEnumerator waitIdle() {
        while (!idle)
            yield return null;
    }

    protected virtual void Awake() {
        GridOrientationStrategy strategy = GetComponent<GridOrientationStrategy>();
        if (strategy != null)
            m_gridOrientation = strategy;
        else
            m_gridOrientation = new GridOrientationStrategyNorth();  
    }

    // Use this for initialization
    protected virtual void Start() {
        if (m_currentWP == null)
            findCurrentPosition();

        WayPoint wp = getCurrentWayPoint();
        if (wp == null)
            Debug.LogError("Item " + name + " is out of grid");
        else {    
            if (gameItem != null)       
                wp.onEnter(gameItem);
        }
	}

    // Update is called once per frame
    protected virtual void Update () {
        //rotate to moving direction
        if (!m_completeRotation) {
            transform.rotation = Quaternion.RotateTowards(transform.rotation, rotateTo, m_rotateSpeed * Time.deltaTime);
            if (transform.rotation == rotateTo) {
                m_completeRotation = true;
                fireRotateStop();
                if (m_targetWP != null)
                    fireMovingStart();
            }
        }
        else if (m_targetWP != null) {
            //make moving            
            transform.position = Vector3.MoveTowards(transform.position, m_targetWP.transform.position, m_moveSpeed * Time.deltaTime);
            //check moving is finished
            if (transform.position == m_targetWP.transform.position) {
                //finish moving and change current position

                getCurrentWayPoint().onExit(gameItem);
                m_currentWP = m_targetWP;
                m_targetWP = null;
                transform.parent = m_currentWP.transform;
                getCurrentWayPoint().onEnter(gameItem);

                fireMovingStop();
            }
        }        
    }

    /**
     * return current WayPoint or null, if not found
     */
    public WayPoint getCurrentWayPoint() {
        if (m_currentWP == null)
            findCurrentPosition();
        return m_currentWP;//Level.inst.grid.getNode(m_currentPosition);
    }

    private Quaternion getRotateTarget(WayPoint wp) {
        return Quaternion.Euler(
            transform.rotation.eulerAngles.x, 
            m_gridOrientation.getAngle(getCurrentWayPoint().transform.position, wp.transform.position), 
            transform.rotation.eulerAngles.z);
    }

    /**
     * Immediatly start movement to WayPoint
     */
    public void rotateAndMoveToImmediatly(WayPoint wayPoint, float moveSpeed, float rotateSpeed) {
        rotateTo = getRotateTarget(wayPoint);
        if (rotateTo == transform.rotation) {
            m_completeRotation = true;
            fireMovingStart();
        }
        else {
            m_completeRotation = false;
            fireRotateStart();   
        }

        m_targetWP = wayPoint;
        //Debug.Log(string.Format("Move from {0} to {1}", m_currentPosition.ToString(), m_targetMovePosition.ToString()));
        m_moveSpeed = moveSpeed;
        m_rotateSpeed = rotateSpeed;
    }

    public void moveToImmediatly(WayPoint wayPoint, float moveSpeed) {
        m_completeRotation = true;
        m_targetWP = wayPoint;
        m_moveSpeed = moveSpeed;
    }

    public void rotateToImmeadiatly(WayPoint wayPoint, float rotateSpeed) {
        m_targetWP = null;
        m_rotateSpeed = rotateSpeed;

        rotateTo = getRotateTarget(wayPoint);
        if (rotateTo == transform.rotation)
            m_completeRotation = true;
        else {
            m_completeRotation = false;
            fireRotateStart();
        }
    }

    protected virtual void fireMovingStart() {
        if (onStartMovingEvent != null)
            onStartMovingEvent(this);
    }

    protected virtual void fireMovingStop() {
        if (onStopMovingEvent != null)
            onStopMovingEvent(this);
    }

    protected virtual void fireRotateStart() {
        if (onStartRotationEvent != null)
            onStartRotationEvent(this);
    }

    protected virtual void fireRotateStop() {
        if (onStopRotationEvent != null)
            onStopRotationEvent(this);
    }

    public void replaceImmeadiatly(WayPoint wp) {        
        m_targetWP = null;
        if (! wp.Equals(getCurrentWayPoint())) {
            getCurrentWayPoint().onExit(gameItem);
            m_currentWP = wp;
            transform.parent = m_currentWP.transform;
            transform.position = wp.transform.position;
            getCurrentWayPoint().onEnter(gameItem);
        }
    }

    protected void updateCurrentPosition() {
        GridPosition np = Level.inst.grid.getGridPosition(transform.position);       
        if (! np.Equals(m_currentWP.gridPosition)) {
            getCurrentWayPoint().onExit(gameItem);
            m_currentWP = Level.inst.grid.getNode(np);
            transform.parent = m_currentWP.transform;
            getCurrentWayPoint().onEnter(gameItem);
        }
    }

    private void findCurrentPosition() {
        GridPosition gp = Level.inst.grid.getGridPosition(transform.position);
        m_currentWP = Level.inst.grid.getNode(gp);
        transform.parent = m_currentWP.transform;
    }

    /// <summary>
    /// current item orientation direction
    /// </summary>
    /// <returns>direction</returns>
    public WayPoint.DIRECTION getLookingDirection() {
        return m_gridOrientation.getLookingDirection(transform.rotation.eulerAngles);       
    }
}
