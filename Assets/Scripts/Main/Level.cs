﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Level : MonoBehaviour {

    public Grid grid;
    public Player player;
    public Respawn lastRespawn;

    private static Level m_instance = null;
    static public Level inst {get {return m_instance;}}

    //blocking count, = 0 game still running, > 0 game paused for some action
    private int m_blockCount = 0;
    //players turn
    private bool m_isPlayerTurn = true;

    //list of items, which waiting his turn
    private LinkedList<TurnBasedI> m_waitingUnits = new LinkedList<TurnBasedI>();
    //list of items, which turn is over
    private LinkedList<TurnBasedI> m_turnOverUnits = new LinkedList<TurnBasedI>();

    void Awake() {
        m_instance = this;
    }

	// Use this for initialization
	void Start () {
        //GridOrientationStrategy s = new GridOrientationStrategyNorth();
        //Debug.Log(s.getAngle(new Vector3(0, 0, 0), new Vector3(0, 0, 1)).ToString());
        //Debug.Log(s.getAngle(new Vector3(0, 0, 0), new Vector3(1, 0, 0)).ToString());
        //Debug.Log(s.getAngle(new Vector3(0, 0, 0), new Vector3(0, 0, -1)).ToString());
        //Debug.Log(s.getAngle(new Vector3(0, 0, 0), new Vector3(-1, 0, 0)).ToString());
        //yield return null;
        startPlayerTurn();
    }
	
	// Update is called once per frame
	void Update () {
	}

    public bool isCanMovePlayer() {
        return m_blockCount == 0 && m_isPlayerTurn;
    }

    /// <summary>
    /// block game (pause)
    /// </summary>
    public void blockGame() {
        m_blockCount += 1;
    }

    /// <summary>
    /// unblock game
    /// </summary>
    public void unblockGame() {
        if (m_blockCount > 0)
            m_blockCount -= 1;
    }

    /// <summary>
    /// register GridItem for making his turn
    /// </summary>
    /// <param name="item">GridItem</param>
    public void registerTurnBasedItem(TurnBasedI item) {
        if (item != player.turnBased) {
            if (m_isPlayerTurn)
                m_turnOverUnits.AddLast(item);
            else
                m_waitingUnits.AddLast(item);
        }
    }

    /// <summary>
    /// unregister GridItem from making turn's items
    /// </summary>
    /// <param name="item">GridItem</param>
    public void unregisterTurnBasedItem(TurnBasedI item) {
        m_waitingUnits.Remove(item);
        m_turnOverUnits.Remove(item);
    }

    /// <summary>
    /// return true if now is item's turn
    /// </summary>
    /// <param name="item">GridItem</param>
    /// <returns>true if item's turn</returns>
    public bool isItemTurn(TurnBasedI item) {
        if (item == player.turnBased) {
            return m_isPlayerTurn;
        } else {
            if (m_waitingUnits.Count == 0)
                return false;
            return m_waitingUnits.First.Value == item;
        }
    }

    /// <summary>
    /// notify game GridItem was complete his turn
    /// </summary>
    /// <param name="item"></param>
    public void turnOver(TurnBasedI item) {        
        if (! isItemTurn(item))
            throw new System.Exception(item.ToString() + "over turn in player turn's time");

        if (m_isPlayerTurn) {
            playerTurnOver();
        } else {
            itemTurnOver();
        }
    }

    /// <summary>
    /// player turn is over
    /// </summary>
    private void playerTurnOver() {
        m_isPlayerTurn = false;
        performNextItemTurn();
    }

    /// <summary>
    /// first waiting item's turn is over
    /// </summary>
    private void itemTurnOver() {
        LinkedListNode<TurnBasedI> item = m_waitingUnits.First;
        m_waitingUnits.RemoveFirst();
        m_turnOverUnits.AddLast(item);

        performNextItemTurn();
    }

    /// <summary>
    /// extract first waiting turn's item and perform his turn
    /// if have not waiting's items, then start player turn
    /// </summary>
    private void performNextItemTurn() {
        if (m_waitingUnits.Count == 0) {
            startPlayerTurn();
            return;
        }
        
        m_waitingUnits.First.Value.startTurn();
    }

    /// <summary>
    /// starting player turn
    /// </summary>
    private void startPlayerTurn() {
        //while (m_turnOverUnits.Count != 0) {
        //  m_waitingUnits.AddLast(m_turnOverUnits.First);
        //m_turnOverUnits.RemoveFirst();
        //}
        //move turn over items to waiting list
        LinkedList<TurnBasedI> tmp = m_turnOverUnits;
        m_turnOverUnits = m_waitingUnits;
        m_waitingUnits = tmp;

        m_isPlayerTurn = true;
        player.startTurn();
    }
}
