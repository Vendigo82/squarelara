﻿using GridPosition = VenSoft.Grid.GridPosition;

/// <summary>
/// 2D dimension grid
/// </summary>
public class SimpleGrid2D : GridI {
    //way points two-dimension array, first index for x, second index for y
    private WayPoint [,]array;
    //array column starting index
    private int colStartingIndex;
    //array row starting index
    private int rowStartingIndex;

    /// <summary>
    /// create grid with column indexes[colStartingIndex, colCount - colStartingIndex - 1] and
    /// row indexes[rowStartingIndex, rowCount - rowStartingIndex - 1] 
    /// </summary>
    /// <param name="colCount">count of columns</param>
    /// <param name="rowCount">count of rows</param>
    /// <param name="colStartingIndex">column starting index</param>
    /// <param name="rowStartingIndex">row starting index</param>
    public SimpleGrid2D(int colCount, int rowCount, int colStartingIndex, int rowStartingIndex) {
        this.colStartingIndex = colStartingIndex;
        this.rowStartingIndex = rowStartingIndex;
        array = new WayPoint[colCount, rowCount];      
    }

    /// <summary>
    /// Adding way point to array in way point.gridPosition
    /// </summary>
    /// <param name="wayPoint"></param>
    public void addWayPoint(WayPoint wayPoint) {      
        int x = wayPoint.gridPosition.x;
        int y = wayPoint.gridPosition.y;
        if (checkIndexesIsValid(x, y))
            array[x - colStartingIndex, y - rowStartingIndex] = wayPoint;
        else
            throw new System.IndexOutOfRangeException("WayPoint " + wayPoint.ToString() + " is out of grid");
    }

    /// <summary>
    /// remove way point from position x,y
    /// </summary>
    /// <param name="x">x index</param>
    /// <param name="y">y index</param>
    public void removeWayPoint(int x, int y) {
        array[x - colStartingIndex, y - rowStartingIndex] = null;
    }

    /**
     * GridI interface
     * Return way point by indexes, or return null if not exist
     */
    public WayPoint getNode(GridPosition p) {
        return getNode(p.x, p.y, p.z);
    }

    /**
     * Return way point by indexes, or return null if not exist
     */
    public WayPoint getNode(int x, int y, int z) {
        if (!checkIndexesIsValid(x, y, z))
            return null;

        WayPoint wayPoint = array[x - colStartingIndex, y - rowStartingIndex ];
        if (wayPoint != null && wayPoint.gameObject.activeSelf && wayPoint.enabled)
            return wayPoint;
        else
            return null;
    }


    /**
     * GridI interface
     * Return waypoint for specific direction or null
     */
    public WayPoint getNorthWP(WayPoint wp) {
        if (wp.noNorth)
            return null;
        GridPosition p = wp.gridPosition;
        return getNode(p.x, p.y + 1, p.z);
    }

    /**
     * GridI interface
     * Return waypoint for specific direction or null
     */
    public WayPoint getSouthWP(WayPoint wp) {
        if (wp.noSouth)
            return null;
        GridPosition p = wp.gridPosition;
        return getNode(p.x, p.y - 1, p.z);
    }

    /**
     * GridI interface
     * Return waypoint for specific direction or null
     */
    public WayPoint getWestWP(WayPoint wp) {
        if (wp.noWest)
            return null;
        GridPosition p = wp.gridPosition;
        return getNode(p.x - 1, p.y, p.z);
    }

    /**
     * GridI interface
     * Return waypoint for specific direction or null
     */
    public WayPoint getEastWP(WayPoint wp) {
        if (wp.noEast)
            return null;
        GridPosition p = wp.gridPosition;
        return getNode(p.x + 1, p.y, p.z);
    }

    /// <summary>
    /// check indexes in array area
    /// </summary>
    /// <param name="x">x index</param>
    /// <param name="y">y index</param>
    /// <returns>true, in indexes is valid; return false if indexes is out of bounds</returns>
    private bool checkIndexesIsValid(int x, int y) {
        x = x - colStartingIndex;
        y = y - rowStartingIndex;

        if (x < 0 || x >= array.GetLength(0))
            return false;
        if (y < 0 || y >= array.GetLength(1))
            return false;
        return true;
    }

    /// <summary>
    /// check indexes in array area
    /// </summary>
    /// <param name="x">x index</param>
    /// <param name="y">y index</param>
    /// <param name="z">z index</param>
    /// <returns>true, in indexes is valid; return false if indexes is out of bounds</returns>
    private bool checkIndexesIsValid(int x, int y, int z) {
        if (z != 0)
            return false;
        return checkIndexesIsValid(x, y);
    }
}
