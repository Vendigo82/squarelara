﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VenSoft.Grid;

/// <summary>
/// Game grid
/// </summary>
public class Grid : MonoBehaviour, GridI {

    //grid step size
    public float gridStep = 5;

    //main grid
    private SimpleGrid2D grid = null;
    private HashSet<WayPoint> notUsingWaypoints = new HashSet<WayPoint>();

    void Awake() {        
        initializeGrid();        
    }

	// Use this for initialization
	void Start () {        
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    /**
     * GridI interface
     * Return way point by indexes, or return null if not exist
     */
    public WayPoint getNode(GridPosition pos) {
        return grid.getNode(pos);
    }

    /**
     * Return way point by indexes, or return null if not exist
     */
    public WayPoint getNode(int x, int y, int z) {
        return grid.getNode(x, y, z);
    }


    /**
     * GridI interface
     * Return waypoint for specific direction or null
     */
    public WayPoint getNorthWP(WayPoint wp) {
        return grid.getNorthWP(wp);
    }

    /**
     * GridI interface
     * Return waypoint for specific direction or null
     */
    public WayPoint getSouthWP(WayPoint wp) {
        return grid.getSouthWP(wp);
    }

    /**
     * GridI interface
     * Return waypoint for specific direction or null
     */
    public WayPoint getWestWP(WayPoint wp) {
        return grid.getWestWP(wp);
    }

    /**
     * GridI interface
     * Return waypoint for specific direction or null
     */
    public WayPoint getEastWP(WayPoint wp) {
        return grid.getEastWP(wp);
    }

    /// <summary>
    /// use global position calculate grid indexes
    /// </summary>
    /// <param name="position">global position</param>
    /// <returns>grid indexes</returns>
    public GridPosition getGridPosition(Vector3 position) {
        GridPosition gridPosition;
        gridPosition.x = Mathf.RoundToInt(position.x / gridStep);   //world x-direction to x index
        gridPosition.y = Mathf.RoundToInt(position.z / gridStep);   //world z-direction to y index
        if (Math.Abs(position.y - transform.position.y) < gridStep) //world y-direction to z index
            gridPosition.z = 0;
        else
            gridPosition.z = 1; //any digit != 0, mark as notUsing
        return gridPosition;
    }       

    /**
     * GridInfoI interface
     * return grid step size
     */
    public float getGridStep() {
        return gridStep;
    }

    /// <summary>
    /// update WayPoint gridPosition position
    /// </summary>
    /// <param name="wp">WayPoint</param>
    public void updateWayPointPosition(WayPoint wp) {
        if (notUsingWaypoints.Contains(wp))
            notUsingWaypoints.Remove(wp);
        else
            grid.removeWayPoint(wp.gridPosition.x, wp.gridPosition.y);
        wp.initIndexes(this);
        registerWayPoint(wp);
    }

    /// <summary>
    /// update objects childs WayPoints
    /// </summary>
    /// <param name="o">object for update his child WayPoints</param>
    public void updateChildWayPointsForObject(GameObject o) {
        WayPoint[] points = o.GetComponentsInChildren<WayPoint>(true);
        foreach (WayPoint p in points) {
            updateWayPointPosition(p);
        }
    }

    /// <summary>
    /// register way point for using
    /// </summary>
    /// <param name="wp">way point</param>
    private void registerWayPoint(WayPoint wp) {
        if (wp.gridPosition.z == 0)
            grid.addWayPoint(wp);
        else
            notUsingWaypoints.Add(wp);
    }

    /**
     * Initialize grid use childrens objects
     */
    private void initializeGrid() {
        bool first = true;
        int maxX = 0, minX = 0, maxY = 0, minY = 0;

        WayPoint[] ma = GameObject.FindObjectsOfType<WayPoint>();//GetComponentsInChildren<WayPoint>();

        for (int i = 0; i < ma.Length; ++i) {    
            ma[i].initIndexes(this);
            if (ma[i].gameObject.activeSelf) {
                if (first) {
                    first = false;
                    maxX = ma[i].gridPosition.x;
                    maxY = ma[i].gridPosition.y;
                    minX = maxX;
                    minY = maxY;
                } else {
                    maxX = Mathf.Max(maxX, ma[i].gridPosition.x);
                    minX = Mathf.Min(minX, ma[i].gridPosition.x);
                    maxY = Mathf.Max(maxY, ma[i].gridPosition.y);
                    minY = Mathf.Min(minY, ma[i].gridPosition.y);
                }
            }
        }

        grid = new SimpleGrid2D(maxX - minX + 1, maxY - minY + 1, minX, minY);
        for (int i = 0; i < ma.Length; ++i) {
            registerWayPoint(ma[i]);
        }
    }

    void OnDrawGizmos() {
        Gizmos.color = Color.red;

        WayPoint []ma = GameObject.FindObjectsOfType<WayPoint>(); ;
        for (int i = 0; i < ma.Length; ++i) { 
            if (ma[i].enabled) {
                if (!ma[i].noEast)
                    Gizmos.DrawLine(ma[i].transform.position, ma[i].transform.position + new Vector3(gridStep/2, 0, 0));
                if (!ma[i].noWest)
                    Gizmos.DrawLine(ma[i].transform.position, ma[i].transform.position + new Vector3(-gridStep/2, 0, 0));
                if (!ma[i].noNorth)
                    Gizmos.DrawLine(ma[i].transform.position, ma[i].transform.position + new Vector3(0, 0, gridStep/2));
                if (!ma[i].noSouth)
                    Gizmos.DrawLine(ma[i].transform.position, ma[i].transform.position + new Vector3(0, 0, -gridStep/2));            
            }
        }
    }
}
