﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using VenSoft.Grid;

/// <summary>
/// Game way point
/// </summary>
public class WayPoint : MonoBehaviour, NodeI<WayPoint> {

    public bool noNorth = false;
    public bool noSouth = false;
    public bool noEast = false;
    public bool noWest = false;

    /// <summary>
    /// enumeration of directions
    /// </summary>
    public enum DIRECTION { NULL = -1, EAST = 0, NORTH = 1, WEST = 2, SOUTH = 3};

    public static readonly int[][] DIR_OFFSET = new int[][] {
        new int[] { 1, 0, 0 },
        new int[] { 0, 1, 0 },
        new int[] { -1, 0, 0 }, 
        new int[] { 0, -1, 0 } };

    private GridPosition m_gridPos;     

    //game items, current based on this WayPoint
    private LinkedList<GameItem> m_items = new LinkedList<GameItem>();

    //public LinkedList<GameItem> items {get {return new LinkedList<GameItem>(m_items);}}

    public static int[] getDirectionOffset(DIRECTION direction) {
        if (direction == DIRECTION.NULL)
            return null;

        return DIR_OFFSET[(int)direction];
    }

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public LinkedList<GameItem> getItems() { 
        return new LinkedList<GameItem>(m_items); 
     }

    /**
     * return current grid indexes
     */
    public GridPosition gridPosition {get {return m_gridPos; }}     

    /**
     * initialize grid indexes
     */
    public void initIndexes(Grid grid) {
        m_gridPos = grid.getGridPosition(transform.position);
    }          

    /// <summary>
    /// return direction from this to wp
    /// </summary>
    /// <param name="wp">WayPoint</param>
    /// <returns>direction</returns>
    public DIRECTION getDirectionForPoint(WayPoint wp) {
        Grid grid = Level.inst.grid;
        if (grid.getNorthWP(this) == wp)
            return DIRECTION.NORTH;
        if (grid.getSouthWP(this) == wp)
            return DIRECTION.SOUTH;
        if (grid.getEastWP(this) == wp)
            return DIRECTION.EAST;
        if (grid.getWestWP(this) == wp)
            return DIRECTION.WEST;

        return DIRECTION.NULL;
    }

    public override string ToString()
    {
        return string.Format("WayPoint {0}:{1}", m_gridPos.x, m_gridPos.y );
    }

    /**
     * Called when any GridItem entered on this WayPoint
     */
    public void onEnter(GameItem item) {     
        if (! m_items.Contains(item))
            m_items.AddLast(item);

        LinkedList<GameItem> copy = getItems();

        foreach (GameItem i in copy)
            if (i != item)
                i.onEnterItem(item);        
    }

    /**
     * Called when any GridItem exit from this WayPoint
     */
    public void onExit(GameItem item) {    
        LinkedList<GameItem> copy = getItems();

        foreach (GameItem i in copy)
            if (i != item)
                i.onExitItem(item);        

        m_items.Remove(item);
    }

    public IEnumerator<WayPoint> GetEnumerator() {
        return new WayPointEnumerator(this);
    }

    IEnumerator IEnumerable.GetEnumerator() {
        return new WayPointEnumerator(this);
    }

    /// <summary>
    /// Enumerator for neighbour nodes
    /// </summary>
    private class WayPointEnumerator : IEnumerator<WayPoint> {
        //index offsets for neighbour nodes: right, up, left, down
        //private static readonly int[,] offset = new int[,] { { 1, 0 }, { 0, 1 }, { -1, 0 }, { 0, -1 } };
        //parent node
        private readonly WayPoint m_node;
        //current index in offset arrays
        private int index = -1;
        private WayPoint current = null;

        public WayPointEnumerator(WayPoint node) {
            m_node = node;
        }

        public WayPoint Current {
            get {
                return current;
            }
        }

        object IEnumerator.Current {
            get {
                return current;
            }
        }

        public void Dispose() {
        }

        public bool MoveNext() {
            do {
                index += 1;
                if (index >= WayPoint.DIR_OFFSET.GetLength(0))
                    return false;

                current = Level.inst.grid.getNode(
                    m_node.m_gridPos.x + WayPoint.DIR_OFFSET[index][0], 
                    m_node.m_gridPos.y + WayPoint.DIR_OFFSET[index][1], 
                    m_node.m_gridPos.z);
            } while (current == null);
            return true;
        }

        public void Reset() {
            index = -1;
        }
    }
}
