﻿using VenSoft.Grid;

/**
 * Waypoints grid interface, extends base Grid interface, adding directionals 
 */
public interface GridI : GridI<WayPoint> {
    /**
     * Return waypoint for specific direction or null
     */
    WayPoint getNorthWP(WayPoint wp);
    WayPoint getSouthWP(WayPoint wp);
    WayPoint getWestWP(WayPoint wp);
    WayPoint getEastWP(WayPoint wp);


}
        