﻿using UnityEngine;
using VenSoft.Grid;

public interface GridInfoI {
    /**
     * Convert global position to Grid indexes
     */
    GridPosition getGridPosition(Vector3 position);

    /**
     * return grid step size
     */
    float getGridStep();
}
